﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.WebHost.Models;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IEmployeeService
    {
        Task<List<EmployeeShortResponse>> GetEmployeesAsync();
        Task<EmployeeResponse> GetEmployeeByIdAsync(Guid id);
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}