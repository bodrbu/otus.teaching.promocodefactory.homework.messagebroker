﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.WebRazor.Models.Administration;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Services
{
    public class AdministrationService : IAdministrationService
    {
        private readonly HttpClient _client;

        public AdministrationService(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<EmployeeShort>> GetEmployeesAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/employees");
            if (!response.IsSuccessStatusCode) return null;
            var customers =
                JsonConvert.DeserializeObject<List<EmployeeShort>>(await response.Content.ReadAsStringAsync());
            return customers;
        }

        public async Task<Employee> GetEmployeeByIdAsync(Guid id)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/employees/{id}");
            if (!response.IsSuccessStatusCode) return null;
            var customer = JsonConvert.DeserializeObject<Employee>(await response.Content.ReadAsStringAsync());
            return customer;
        }

        public async Task UpdateAppliedPromocodesAsync(Guid employeeId)
        {
            await _client.PostAsync($"{_client.BaseAddress}api/v1/Employees/{employeeId}/appliedPromocodes", new StringContent(""));
        }

        public async Task<List<Role>> GetRolesAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/roles");
            if (!response.IsSuccessStatusCode) return null;
            var customers =
                JsonConvert.DeserializeObject<List<Role>>(await response.Content.ReadAsStringAsync());
            return customers;
        }
    }
}