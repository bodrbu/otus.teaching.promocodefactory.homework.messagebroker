﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.WebRazor.Models;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;

namespace Otus.Teaching.Pcf.WebRazor.Services.Abstractions
{
    public interface IGivingToCustomerService
    {
        Task<List<CustomerShort>> GetAllAsync();
        Task<Customer> GetCustomerAsync(Guid id);
        Task CreateCustomerAsync(CreateOrEditCustomerRequest request);
        Task UpdateCustomerAsync(Guid id, CreateOrEditCustomerRequest request);
        Task DeleteCustomerAsync(Guid id);

        Task<List<PromoCodeShort>> GetAllPromocodesAsync();
        Task GivePromoCodesToCustomersAsync(GivePromoCodeRequest request);

        Task<List<Preference>> GetAllPreferences();

    }
}