﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.WebRazor.Models;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Customers
{
    public class Details : PageModel
    {
        private readonly IGivingToCustomerService _givingService;

        public Details(IGivingToCustomerService givingService)
        {
            _givingService = givingService;
        }

        public Customer Customer { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            Customer = await _givingService.GetCustomerAsync(id);
            return Page();
        }
    }
}