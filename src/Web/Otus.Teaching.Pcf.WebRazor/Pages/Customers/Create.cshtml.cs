﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Customers
{
    public class CreateModel : PageModel
    {
        private readonly IGivingToCustomerService _givingService;

        public CreateModel(IGivingToCustomerService givingService)
        {
            _givingService = givingService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public List<SelectListItem> ListItems { get; set; }

        public async Task OnGetAsync()
        {
            var preferences = await _givingService.GetAllPreferences();

            ListItems = preferences
                .Select(preference => new SelectListItem
                {
                    Value = preference.Id.ToString(),
                    Text = preference.Name
                }).ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var request = Input.Adapt<CreateOrEditCustomerRequest>();
            request.PreferenceIds = Input.SelectedValues.Select(Guid.Parse).ToList();
            await _givingService.CreateCustomerAsync(request);

            return RedirectToPage(nameof(Index));
        }


        public class InputModel
        {
            [Required]
            [Display(Name = "Имя")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Имя")]
            public string LastName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Адрес электронной почты")]
            public string Email { get; set; }

            public IEnumerable<string> SelectedValues { get; set; }= new List<string>();
        }
    }
}