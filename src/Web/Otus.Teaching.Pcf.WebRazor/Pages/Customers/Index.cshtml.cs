﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.WebRazor.Models;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Customers
{
    public class Index : PageModel
    {
        private readonly IGivingToCustomerService _givingService;

        public Index(IGivingToCustomerService givingService)
        {
            _givingService = givingService;
        }

        public List<CustomerShort> Customers { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Customers = await _givingService.GetAllAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            await _givingService.DeleteCustomerAsync(id);
            return RedirectToPage();
        }
    }
}