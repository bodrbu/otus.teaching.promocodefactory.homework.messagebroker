﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.WebRazor.Models.Receiving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Partners
{
    public class Index : PageModel
    {
        private readonly IReceivingFromPartnerService _receivingFromPartnerService;

        public Index(IReceivingFromPartnerService receivingFromPartnerService)
        {
            _receivingFromPartnerService = receivingFromPartnerService;
        }

        public List<Partner> Partners { get; set; }

        public async Task<IActionResult> OnGet()
        {
            Partners = await _receivingFromPartnerService.GetPartnersAsync();
            return Page();
        }
    }
}