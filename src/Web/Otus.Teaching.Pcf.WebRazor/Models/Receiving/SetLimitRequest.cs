﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Receiving
{
    public class SetLimitRequest
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}