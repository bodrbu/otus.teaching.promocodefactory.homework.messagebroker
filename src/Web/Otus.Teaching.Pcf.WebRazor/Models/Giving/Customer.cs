﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.WebRazor.Models.Giving
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Preference> Preferences { get; set; }
        public List<PromoCodeShort> PromoCodes { get; set; }
    }

    public class CustomerShort
    {
        public Guid Id { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Почта")]
        public string Email { get; set; }
    }
}