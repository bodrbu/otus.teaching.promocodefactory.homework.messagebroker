﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Administration
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}