﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Administration
{
    public class EmployeeShort
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
    }
}