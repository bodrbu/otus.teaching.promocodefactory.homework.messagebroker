﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Pcf.Integration.Contracts;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Gateways
{
    public class GivingPromocodeToCustomerBrokerGateway : IGivingPromoCodeToCustomerGateway
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public GivingPromocodeToCustomerBrokerGateway(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }


        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _publishEndpoint.Publish<GivePromoCodeToCustomer>(new
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                PromoCodeId = promoCode.Id,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            });
        }
    }
}