﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromocodesService _promocodesService;

        public PromocodesController(IPromocodesService promocodesService)
        {
            _promocodesService = promocodesService;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            try
            {
                var response = await _promocodesService.GetAllAsync();
                return Ok(response);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            try
            {
                await _promocodesService.GivePromoCodesToCustomersAsync(request);
                return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest();
            }
        }
    }
}