﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IPromocodesService
    {
        public Task<List<PromoCodeShortResponse>> GetAllAsync();
        public Task GivePromoCodesToCustomersAsync(GivePromoCodeRequest request);
    }
}